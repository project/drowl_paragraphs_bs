<?php

namespace Drupal\Tests\drowl_paragraphs_bs_type_layout\Functional;

use Drupal\Tests\drowl_paragraphs_bs\Functional\GenericDrowlParagraphsBsTestBase;

/**
 * Generic module tests for drowl_paragraphs_bs_type_layout.
 *
 * @group drowl_paragraphs_bs_type_layout
 */
class GenericDrowlParagraphsBsLayoutTest extends GenericDrowlParagraphsBsTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'drowl_paragraphs_bs',
    'media',
    'media_library',
    'fences',
  ];

}
