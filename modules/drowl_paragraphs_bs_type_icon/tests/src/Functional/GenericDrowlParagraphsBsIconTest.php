<?php

namespace Drupal\Tests\drowl_paragraphs_bs_type_icon\Functional;

use Drupal\Tests\drowl_paragraphs_bs\Functional\GenericDrowlParagraphsBsTestBase;

/**
 * Generic module tests for drowl_paragraphs_bs_type_icon.
 *
 * @group drowl_paragraphs_bs_type_icon
 */
class GenericDrowlParagraphsBsIconTest extends GenericDrowlParagraphsBsTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'drowl_paragraphs_bs',
    'link_attributes',
    'micon',
  ];

}
