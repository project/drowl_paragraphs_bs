<?php

namespace Drupal\Tests\drowl_paragraphs_bs_type_view\Functional;

use Drupal\Tests\drowl_paragraphs_bs\Functional\GenericDrowlParagraphsBsTestBase;

/**
 * Generic module tests for drowl_paragraphs_bs_type_view.
 *
 * @group drowl_paragraphs_bs_type_view
 */
class GenericDrowlParagraphsBsViewTest extends GenericDrowlParagraphsBsTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'drowl_paragraphs_bs',
    'views',
    'viewsreference',
  ];

}
