<?php

namespace Drupal\Tests\drowl_paragraphs_bs_type_gallery\Functional;

use Drupal\Tests\drowl_paragraphs_bs\Functional\GenericDrowlParagraphsBsTestBase;

/**
 * Generic module tests for drowl_paragraphs_bs_type_gallery.
 *
 * @group drowl_paragraphs_bs_type_gallery
 */
class GenericDrowlParagraphsBsGalleryTest extends GenericDrowlParagraphsBsTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'drowl_paragraphs_bs',
    'fences',
    'field_formatter',
    'media',
    'media_library',
    'media_library_edit',
  ];

}
