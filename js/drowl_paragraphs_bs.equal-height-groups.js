/**
 * @file
 * DROWL Paragraphs frontend JavaScript
 * Credits: https://dev.to/dhruvangg/equal-height-in-vanilla-javascript-49ch
 */
(function ($, Drupal) {
  function setHeight(el, val) {
    if (typeof val === "string") {
      el.style.setProperty("--equal-height-group-height", val);
    } else {
      el.style.setProperty("--equal-height-group-height", val + "px");
      el.classList.add("paragraph--height-equalized");
    }
  }
  const equalHeight = function (nodeList) {
    let currentTallest = 0;
    let currentRowStart = 0;
    const rowDivs = [];
    let topPosition = 0;
    Array.from(nodeList).forEach((el) => {
      el.style.setProperty("--equal-height-group-height", "0px");
      el.classList.remove("paragraph--height-equalized");
      topPosition = el.offsetTop;
      if (currentRowStart !== topPosition) {
        for (let i = 0; i < rowDivs.length; i++) {
          setHeight(rowDivs[i], currentTallest);
        }
        rowDivs.length = 0;
        currentRowStart = topPosition;
        currentTallest = parseFloat(
          getComputedStyle(el, null).height.replace("px", "")
        );
        rowDivs.push(el);
      } else {
        rowDivs.push(el);
        currentTallest =
          currentTallest <
          parseFloat(getComputedStyle(el, null).height.replace("px", ""))
            ? parseFloat(getComputedStyle(el, null).height.replace("px", ""))
            : currentTallest;
      }
      for (let i = 0; i < rowDivs.length; i++) {
        setHeight(rowDivs[i], currentTallest);
      }
    });
  };
  function equalizeParagraphsGroupsHeight(context) {
    const equalHeightParagraphs = context.querySelectorAll(
      "[data-equal-height-group]"
    );
    const equalHeightGroups = {};
    if (equalHeightParagraphs.length) {
      for (let i = 0; i < equalHeightParagraphs.length; i++) {
        equalHeightGroups[equalHeightParagraphs[i].dataset.equalHeightGroup] =
          context.querySelectorAll(
            `[data-equal-height-group='${equalHeightParagraphs[i].dataset.equalHeightGroup}']`
          );
      }
      Object.values(equalHeightGroups).forEach((nodelist) => {
        equalHeight(nodelist);
      });
    }
  }
  Drupal.behaviors.drowl_paragraphs_bs_equal_height_groups = {
    attach(context) {
      equalizeParagraphsGroupsHeight(context);
      // Fire again if the document is fully loaded
      window.addEventListener(
        "load",
        equalizeParagraphsGroupsHeight(context),
        false
      );

      // Fire on document resize
      let prevWidth = 0;
      new ResizeObserver(entries => {
        const width = entries[0].borderBoxSize?.[0].inlineSize;
        if (typeof width === 'number' && width !== prevWidth) {
          equalizePragraphsGroupsHeight(context);
        }
        prevWidth = width;
      }).observe(document.body);
    },
  };
})(jQuery, Drupal);
