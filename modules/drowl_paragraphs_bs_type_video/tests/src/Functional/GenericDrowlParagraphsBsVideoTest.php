<?php

namespace Drupal\Tests\drowl_paragraphs_bs_type_video\Functional;

use Drupal\Tests\drowl_paragraphs_bs\Functional\GenericDrowlParagraphsBsTestBase;

/**
 * Generic module tests for drowl_paragraphs_bs_type_video.
 *
 * @group drowl_paragraphs_bs_type_video
 */
class GenericDrowlParagraphsBsVideoTest extends GenericDrowlParagraphsBsTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'drowl_paragraphs_bs',
    'media',
    'media_library',
    'media_library_edit',
  ];

}
