<?php

namespace Drupal\Tests\drowl_paragraphs_bs_type_markup\Functional;

use Drupal\Tests\drowl_paragraphs_bs\Functional\GenericDrowlParagraphsBsTestBase;

/**
 * Generic module tests for drowl_paragraphs_bs_type_markup.
 *
 * @group drowl_paragraphs_bs_type_markup
 */
class GenericDrowlParagraphsBsMarkupTest extends GenericDrowlParagraphsBsTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'drowl_paragraphs_bs',
  ];

}
