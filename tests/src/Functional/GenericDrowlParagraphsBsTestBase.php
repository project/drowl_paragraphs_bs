<?php

namespace Drupal\Tests\drowl_paragraphs_bs\Functional;

use Drupal\Tests\system\Functional\Module\GenericModuleTestBase;

/**
 * Generic module tests for drowl_paragraphs_bs.
 *
 * @group drowl_paragraphs_bs
 */
class GenericDrowlParagraphsBsTestBase extends GenericModuleTestBase {

  /**
   * {@inheritDoc}
   *
   * We need to use the standard installation profile here, so we additionally
   * install the core media_types.
   */
  protected $profile = 'standard';

  // @codingStandardsIgnoreStart
  /**
   * {@inheritDoc}
   *
   * @todo, we need to temporarily disable this as both
   * https://www.drupal.org/project/entity_reference_display/issues/3432401
   * and
   * https://www.drupal.org/project/ui_styles_paragraphs/issues/3432164
   * will result in schema errors on which we don't have any impact on.
   */
  protected $strictConfigSchema = FALSE;
  // @codingStandardsIgnoreEnd

  /**
   * {@inheritDoc}
   */
  protected function assertHookHelp(string $module): void {
    // Don't do anything here. Just overwrite this useless method, so we do not.
    // Need to implement a hook_help() method.
  }

}
