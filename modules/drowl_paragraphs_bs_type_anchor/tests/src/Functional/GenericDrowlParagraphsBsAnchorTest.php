<?php

namespace Drupal\Tests\drowl_paragraphs_bs_type_anchor\Functional;

use Drupal\Tests\drowl_paragraphs_bs\Functional\GenericDrowlParagraphsBsTestBase;

/**
 * Generic module tests for drowl_paragraphs_bs_type_anchor.
 *
 * @group drowl_paragraphs_bs_type_anchor
 */
class GenericDrowlParagraphsBsAnchorTest extends GenericDrowlParagraphsBsTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'drowl_paragraphs_bs',
    'fences',
  ];

}
