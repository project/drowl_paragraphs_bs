/**
 * @file
 * DROWL Paragraphs for Bootstrap frontend JavaScript
 */
(function ($, Drupal) {
  Drupal.behaviors.drowl_paragraphs_bs_type_countdown = {
    attach: function (context, settings) {
      $(document).ready(function () {
        $(".countdown", context).each(function () {
          let $countdownWrapper = $(this);
          let $countdownNoJsFallback = $countdownWrapper.next(
            ".countdown-no-js-fallback:first"
          );
          let targetTimestamp = $countdownWrapper.data("target-date");
          let countdownTheme = $countdownWrapper.data("countdown-theme");
          let countdownId = $countdownWrapper.attr("id");
          $countdownWrapper.addClass("flipdown");
          new FlipDown(targetTimestamp, countdownId, {
            headings: [
              Drupal.t("Days"),
              Drupal.t("Hours"),
              Drupal.t("Minutes"),
              Drupal.t("Seconds"),
            ],
            theme: countdownTheme,
          })
            .start(function () {
              //
            })
            .ifEnded(() => {
              //
            });
          // Hide NO-JS-Fallback time
          $countdownNoJsFallback.addClass("hidden");
        });
      });
    },
  };
})(jQuery, Drupal);
