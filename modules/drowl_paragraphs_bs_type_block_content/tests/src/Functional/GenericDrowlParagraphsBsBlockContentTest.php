<?php

namespace Drupal\Tests\drowl_paragraphs_bs_type_block_content\Functional;

use Drupal\Tests\drowl_paragraphs_bs\Functional\GenericDrowlParagraphsBsTestBase;

/**
 * Generic module tests for drowl_paragraphs_bs_type_block_content.
 *
 * @group drowl_paragraphs_bs_type_block_content
 */
class GenericDrowlParagraphsBsBlockContentTest extends GenericDrowlParagraphsBsTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'drowl_paragraphs_bs',
    'block_field',
    'block_content',
  ];

}
