<?php

namespace Drupal\drowl_paragraphs_bs\Drush\Commands;

use Drupal\Core\Extension\ExtensionPathResolver;
use Drupal\Core\Extension\ModuleInstallerInterface;
use Drupal\Core\File\FileSystemInterface;
use Drush\Attributes as CLI;
use Drush\Commands\DrushCommands;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides Drush commands for the "drowl_paragraphs_bs" module.
 */
class DrowlParagraphsBsCommands extends DrushCommands {

  /**
   * The module installer service.
   *
   * @var \Drupal\Core\Extension\ModuleInstallerInterface
   */
  protected $moduleInstaller;

  /**
   * The extension path resolver service.
   *
   * @var \Drupal\Core\Extension\ExtensionPathResolver
   */
  protected $extensionPathResolver;

  /**
   * The $fileSystem service.
   *
   * @var Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * Constructs a DrowlParagraphsBsCommands object.
   */
  public function __construct(ModuleInstallerInterface $moduleInstaller, ExtensionPathResolver $extensionPathResolver, FileSystemInterface $fileSystem) {
    $this->moduleInstaller = $moduleInstaller;
    $this->extensionPathResolver = $extensionPathResolver;
    $this->fileSystem = $fileSystem;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('module_installer'),
      $container->get('extension.path.resolver'),
      $container->get('file_system'),
    );
  }

  /**
   * Installs all of the "drowl_paragraphs_bs" submodules.
   */
  #[CLI\Command(name: 'drowl_paragraphs_bs:install-submodules', aliases: ['dpbs-install-sub'])]
  #[CLI\Usage(name: 'drowl_paragraphs_bs:install-submodules', description: 'Installs all of the "drowl_paragraphs_bs" submodules.')]
  #[CLI\Option(name: 'exclude', description: 'Exclude specified submodules. Seperate each submodule by ",".')]
  public function installSubmodules($options = ['exclude' => '']) {
    $submoduleFolderPath = $this->extensionPathResolver->getPath('module', 'drowl_paragraphs_bs') . '/modules';
    $submoduleInfoYmls = $this->fileSystem->scanDirectory($submoduleFolderPath, '/.*\.info.yml/');
    $submoduleNames = [];
    $excludedSubmodules = [];
    if (!empty($options['exclude'])) {
      $excludedSubmodules = array_map('trim', explode(',', $options['exclude']));
    }
    foreach ($submoduleInfoYmls as $submoduleInfoYml) {
      // Get submodule name without ".info":
      $submoduleName = substr($submoduleInfoYml->name, 0, -5);
      // If we specified an excluded submodule, skip it:
      if (in_array($submoduleName, $excludedSubmodules)) {
        continue;
      }
      $submoduleNames[] = $submoduleName;
    }
    if ($this->moduleInstaller->install($submoduleNames)) {
      $this->io()->success('All "drowl_paragraphs_bs" submodules were successfully installed.');
    }
    else {
      $this->io()->error('Failed to install all "drowl_paragraphs_bs" submodules.');
    }
  }

}
