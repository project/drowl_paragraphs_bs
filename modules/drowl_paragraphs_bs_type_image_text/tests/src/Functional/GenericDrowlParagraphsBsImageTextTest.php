<?php

namespace Drupal\Tests\drowl_paragraphs_bs_type_image_text\Functional;

use Drupal\Tests\drowl_paragraphs_bs\Functional\GenericDrowlParagraphsBsTestBase;

/**
 * Generic module tests for drowl_paragraphs_bs_type_image_text.
 *
 * @group drowl_paragraphs_bs_type_image_text
 */
class GenericDrowlParagraphsBsImageTextTest extends GenericDrowlParagraphsBsTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'drowl_paragraphs_bs',
    'link_attributes',
    'media_library',
    'media_library_edit',
    'fences',
    'field_formatter',
    'media',
  ];

}
