<?php

namespace Drupal\Tests\drowl_paragraphs_bs_type_layout_restricted_access\Functional;

use Drupal\Tests\drowl_paragraphs_bs\Functional\GenericDrowlParagraphsBsTestBase;

/**
 * Generic module tests for drowl_paragraphs_bs_type_layout_restricted_access.
 *
 * @group drowl_paragraphs_bs_type_layout_restricted_access
 */
class GenericDrowlParagraphsBsRestrictedAccessTest extends GenericDrowlParagraphsBsTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'drowl_paragraphs_bs',
    'layout_paragraphs',
    'drowl_layouts_bs',
    'media',
    'media_library',
    'media_library_edit',
    'entity_access_by_role_field',
  ];

}
