<?php

namespace Drupal\Tests\drowl_paragraphs_bs_type_layout_slideshow\Functional;

use Drupal\Tests\drowl_paragraphs_bs\Functional\GenericDrowlParagraphsBsTestBase;

/**
 * Generic module tests for drowl_paragraphs_bs_type_layout_slideshow.
 *
 * @group drowl_paragraphs_bs_type_layout_slideshow
 */
class GenericDrowlParagraphsBsLayoutSlideshowTest extends GenericDrowlParagraphsBsTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'drowl_paragraphs_bs',
    'media',
    'media_library',
    'media_library_edit',
  ];

}
