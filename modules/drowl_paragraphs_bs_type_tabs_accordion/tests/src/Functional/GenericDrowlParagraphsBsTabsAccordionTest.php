<?php

namespace Drupal\Tests\drowl_paragraphs_bs_type_tabs_accordion\Functional;

use Drupal\Tests\drowl_paragraphs_bs\Functional\GenericDrowlParagraphsBsTestBase;

/**
 * Generic module tests for drowl_paragraphs_bs_type_tabs_accordion.
 *
 * @group drowl_paragraphs_bs_type_tabs_accordion
 */
class GenericDrowlParagraphsBsTabsAccordionTest extends GenericDrowlParagraphsBsTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'drowl_paragraphs_bs',
    'fences',
    'micon',
  ];

}
