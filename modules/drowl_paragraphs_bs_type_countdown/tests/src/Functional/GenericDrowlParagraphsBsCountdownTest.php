<?php

namespace Drupal\Tests\drowl_paragraphs_bs_type_countdown\Functional;

use Drupal\Tests\drowl_paragraphs_bs\Functional\GenericDrowlParagraphsBsTestBase;

/**
 * Generic module tests for drowl_paragraphs_bs_type_countdown.
 *
 * @group drowl_paragraphs_bs_type_countdown
 */
class GenericDrowlParagraphsBsCountdownTest extends GenericDrowlParagraphsBsTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'drowl_paragraphs_bs',
    'datetime',
  ];

}
