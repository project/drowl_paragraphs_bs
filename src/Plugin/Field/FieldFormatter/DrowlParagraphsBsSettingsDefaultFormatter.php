<?php

namespace Drupal\drowl_paragraphs_bs\Plugin\Field\FieldFormatter;

use Drupal\Component\Utility\Html;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Field formatter "drowl_paragraphs_bs_settings_default".
 *
 * @FieldFormatter(
 *   id = "drowl_paragraphs_bs_settings_default",
 *   label = @Translation("DROWL Paragraphs for Bootstrap settings default"),
 *   field_types = {
 *     "drowl_paragraphs_bs_settings",
 *   }
 * )
 */
class DrowlParagraphsBsSettingsDefaultFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $output = [];
    return $output;

  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    return $summary;

  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $output = [];
    // Iterate over every field item and build a renderable array for each item.
    foreach ($items as $delta => $item) {
      $build = [
        '#type' => 'container',
        'value' => [],
      ];
      if (!empty($item)) {
        foreach ($item as $key => $value) {
          $build['value'][$key] = [
            '#type' => 'container',
            '#title' => $value->getName(),
            '#attributes' => [
              'class' => Html::cleanCssIdentifier($value->getName()),
            ],
            '#plain_text' => $value->getValue(),
            // '#markup' => t('Name'),
          ];
        }
      }
      $output[$delta] = $build;
    }
    return $output;
  }

}
