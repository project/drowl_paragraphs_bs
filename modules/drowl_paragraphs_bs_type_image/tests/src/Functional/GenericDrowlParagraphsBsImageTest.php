<?php

namespace Drupal\Tests\drowl_paragraphs_bs_type_image\Functional;

use Drupal\Tests\drowl_paragraphs_bs\Functional\GenericDrowlParagraphsBsTestBase;

/**
 * Generic module tests for drowl_paragraphs_bs_type_image.
 *
 * @group drowl_paragraphs_bs_type_image
 */
class GenericDrowlParagraphsBsImageTest extends GenericDrowlParagraphsBsTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'drowl_paragraphs_bs',
    'media',
    'fences',
    'link_attributes',
    'media_library',
    'media_library_edit',
    'drowl_media_types',
    'field_formatter',
  ];

}
