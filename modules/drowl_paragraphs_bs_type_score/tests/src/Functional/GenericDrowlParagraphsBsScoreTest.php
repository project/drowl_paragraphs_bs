<?php

namespace Drupal\Tests\drowl_paragraphs_bs_type_score\Functional;

use Drupal\Tests\drowl_paragraphs_bs\Functional\GenericDrowlParagraphsBsTestBase;

/**
 * Generic module tests for drowl_paragraphs_bs_type_score.
 *
 * @group drowl_paragraphs_bs_type_score
 */
class GenericDrowlParagraphsBsScoreTest extends GenericDrowlParagraphsBsTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'drowl_paragraphs_bs',
    'micon',
  ];

}
