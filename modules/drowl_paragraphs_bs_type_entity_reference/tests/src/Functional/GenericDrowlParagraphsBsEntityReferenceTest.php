<?php

namespace Drupal\Tests\drowl_paragraphs_bs_type_entity_reference\Functional;

use Drupal\Tests\drowl_paragraphs_bs\Functional\GenericDrowlParagraphsBsTestBase;

/**
 * Generic module tests for drowl_paragraphs_bs_type_entity_reference.
 *
 * @group drowl_paragraphs_bs_type_entity_reference
 */
class GenericDrowlParagraphsBsEntityReferenceTest extends GenericDrowlParagraphsBsTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'drowl_paragraphs_bs',
    'fences',
    'entity_reference_display',
    'node',
  ];

}
