<?php

namespace Drupal\Tests\drowl_paragraphs_bs_type_text\Functional;

use Drupal\Tests\drowl_paragraphs_bs\Functional\GenericDrowlParagraphsBsTestBase;

/**
 * Generic module tests for drowl_paragraphs_bs_type_text.
 *
 * @group drowl_paragraphs_bs_type_text
 */
class GenericDrowlParagraphsBsTextTest extends GenericDrowlParagraphsBsTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'drowl_paragraphs_bs',
  ];

}
