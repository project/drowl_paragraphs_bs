<?php

namespace Drupal\Tests\drowl_paragraphs_bs_type_button\Functional;

use Drupal\Tests\drowl_paragraphs_bs\Functional\GenericDrowlParagraphsBsTestBase;

/**
 * Generic module tests for drowl_paragraphs_bs_type_button.
 *
 * @group drowl_paragraphs_bs_type_button
 */
class GenericDrowlParagraphsBsButtonTest extends GenericDrowlParagraphsBsTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'drowl_paragraphs_bs',
    'fences',
    'micon_link',
  ];

}
