<?php

namespace Drupal\Tests\drowl_paragraphs_bs\Functional;

use Drupal\Tests\drowl_paragraphs_bs\Functional\GenericDrowlParagraphsBsTestBase;

/**
 * Generic module tests for drowl_paragraphs_bs.
 *
 * @group drowl_paragraphs_bs
 */
class GenericDrowlParagraphsBsTest extends GenericDrowlParagraphsBsTestBase {

  /**
   * {@inheritDoc}
   */
  protected function preUnInstallSteps(): void {
    // Programmatically delete the field storage, so we can uninstall the
    // module:
    \Drupal::service('config.factory')->getEditable('field.storage.paragraph.field_settings')->delete();
  }

}
