<?php

namespace Drupal\Tests\drowl_paragraphs_bs_type_card\Functional;

use Drupal\Tests\drowl_paragraphs_bs\Functional\GenericDrowlParagraphsBsTestBase;

/**
 * Generic module tests for drowl_paragraphs_bs_type_card.
 *
 * @group drowl_paragraphs_bs_type_card
 */
class GenericDrowlParagraphsBsCardTest extends GenericDrowlParagraphsBsTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'media',
    'drowl_paragraphs_bs',
    'fences',
    'link_attributes',
    'media_library',
    'media_library_edit',
    'field_formatter',
  ];

}
