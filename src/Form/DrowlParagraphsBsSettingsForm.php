<?php

namespace Drupal\drowl_paragraphs_bs\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\OptGroup;

/**
 * Administration settings form.
 */
class DrowlParagraphsBsSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'drowl_paragraphs_bs_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('drowl_paragraphs_bs.settings');
    $settings = $config->get();

    $slide_amount_options = array_combine(range(1, 10), range(1, 10));

    $form['defaults'] = [
      '#type' => 'details',
      '#attributes' => [
        'id' => 'drowl-paragraphs-defaults-wrapper',
      ],
      '#title' => $this->t('Defaults'),
      '#description' => $this->t('Set the global defaults for DROWL Paragraphs for Bootstrap.'),
      '#open' => TRUE,
    ];

    $form['defaults']['layout_slideshow'] = [
      '#type' => 'details',
      '#title' => $this->t('Layout Slideshow'),
      '#description' => $this->t('Configuration for paragraph type "Layout Slideshow'),
      '#open' => TRUE,
    ];

    $form['defaults']['layout_slideshow']['layout_section_width'] = [
      '#type' => 'select',
      '#title' => $this->t('Section width'),
      '#options' => [
        'page-width' => $this->t('Page width (all)'),
        'viewport-width' => $this->t('Viewport width (all)'),
        'viewport-width-cp' => $this->t('Viewport width (only background)'),
      ],
      '#default_value' => $settings['defaults']['layout_slideshow']['layout_section_width'],
      '#required' => TRUE,
      '#description' => $this->t('Overrides the container width, ignoring the parent container width. Viewport width = screen width, Page width = content width.'),
    ];
    $form['defaults']['layout_slideshow']['autoplay'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Autoplay'),
      '#default_value' => $settings['defaults']['layout_slideshow']['autoplay'],
      '#description' => $this->t('Plays the slideshow automatically. If this option is deactivated, the user has to forward manually.'),
    ];
    $form['defaults']['layout_slideshow']['auto_height'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Automatic height'),
      '#default_value' => $settings['defaults']['layout_slideshow']['auto_height'],
      '#description' => $this->t('Automatically detects each slides height and accordingly changes the slideshow height on scroll.'),
    ];
    $form['defaults']['layout_slideshow']['navigation_arrows'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show arrows navigation'),
      '#default_value' => $settings['defaults']['layout_slideshow']['navigation_arrows'],
      '#description' => $this->t('Shows back / forward navigation arrows.'),
    ];
    $form['defaults']['layout_slideshow']['navigation_dots'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show dots navigation'),
      '#default_value' => $settings['defaults']['layout_slideshow']['navigation_dots'],
      '#description' => $this->t('Show navigation dots.'),
    ];
    $form['defaults']['layout_slideshow']['infinite'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Infinite loop'),
      '#default_value' => $settings['defaults']['layout_slideshow']['infinite'],
      '#description' => $this->t('Displays the slideshow infinitely by restarting at the first element after the last.'),
    ];
    $form['defaults']['layout_slideshow']['center_mode'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Center mode'),
      '#default_value' => $settings['defaults']['layout_slideshow']['center_mode'],
      '#description' => $this->t('Allows a centered view by presenting the previous and next slide cropped. For center mode to work, the number of visible slides must be set to an odd number.'),
    ];
    $form['defaults']['layout_slideshow']['controls_outside'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Controls outside'),
      '#default_value' => $settings['defaults']['layout_slideshow']['controls_outside'],
      '#description' => $this->t('Shows the controls (arrows / dots) outside the slider so as not to obscure the content.'),
    ];

    $form['defaults']['layout_slideshow']['visible_elements_sm'] = [
      '#type' => 'select',
      '#title' => $this->t('Visible elements (Small devices)'),
      '#description' => $this->t('Set the number of slide elements to show on small devices'),
      '#default_value' => $settings['defaults']['layout_slideshow']['visible_elements_sm'],
      '#options' => $slide_amount_options,
    ];
    $form['defaults']['layout_slideshow']['visible_elements_md'] = [
      '#type' => 'select',
      '#title' => $this->t('Visible elements (Medium devices)'),
      '#description' => $this->t('Set the number of slide elements to show on medium devices'),
      '#default_value' => $settings['defaults']['layout_slideshow']['visible_elements_md'],
      '#options' => $slide_amount_options,
    ];
    $form['defaults']['layout_slideshow']['visible_elements_lg'] = [
      '#type' => 'select',
      '#title' => $this->t('Visible elements (Large devices)'),
      '#description' => $this->t('Set the number of slide elements to show on large devices'),
      '#default_value' => $settings['defaults']['layout_slideshow']['visible_elements_lg'],
      '#options' => $slide_amount_options,
    ];

    $form['defaults']['breakpoint_sizes'] = [
      '#type' => 'details',
      '#title' => $this->t('Breakpoint values'),
      '#description' => $this->t('Breakpoint values for this site.'),
      '#open' => TRUE,
    ];
    $form['defaults']['breakpoint_sizes']['md'] = [
      '#type' => 'textfield',
      '#size' => 5,
      '#title' => $this->t('Breakpoint Medium devices in px'),
      '#description' => $this->t('A pixel value from which the device size "medium" applies.'),
      '#default_value' => $settings['defaults']['breakpoint_sizes']['md'],
    ];

    $form['defaults']['breakpoint_sizes']['lg'] = [
      '#type' => 'textfield',
      '#size' => 5,
      '#title' => $this->t('Breakpoint Large devices in px'),
      '#description' => $this->t('A pixel value from which the device size "large" applies.'),
      '#default_value' => $settings['defaults']['breakpoint_sizes']['lg'],
    ];
    $form['#tree'] = TRUE;
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->configFactory->getEditable('drowl_paragraphs_bs.settings');
    $form_values = $form_state->getValues();

    $config->set('defaults', $form_values['defaults'])
      ->save();
    parent::submitForm($form, $form_state);
  }

  /**
   * Generates a string representation of an array of 'allowed values'.
   *
   * This string format is suitable for edition in a textarea.
   *
   * @param array $values
   *   An array of values, where array keys are values and array values are
   *   labels.
   *
   * @return string
   *   The string representation of the $values array:
   *    - Values are separated by a carriage return.
   *    - Each value is in the format "value|label" or "value".
   *
   * @see \Drupal\options\Plugin\Field\FieldType\ListItemBase::allowedValuesString()
   */
  protected static function arrayToKeyValueString(array $values = []) {
    $lines = [];
    // Flatten the options to remove possible duplicates:
    $values = OptGroup::flattenOptions($values);
    foreach ($values as $key => $value) {
      $lines[] = "$key|$value";
    }
    return implode("\n", $lines);
  }

  /**
   * Extracts the allowed values array from the given string.
   *
   * @see \Drupal\options\Plugin\Field\FieldType\ListItemBase::extractAllowedValues()
   */
  protected static function keyValueStringToArray($string) {
    $values = [];
    $list = explode("\n", $string);
    $list = array_map('trim', $list);
    $list = array_filter($list, 'strlen');
    foreach ($list as $text) {
      $matches = [];
      if (preg_match('/(.*)\|(.*)/', $text, $matches)) {
        // Trim key and value to avoid unwanted spaces issues.
        $key = trim($matches[1]);
        $value = trim($matches[2]);
      }
      $values[$key] = $value;
    }
    return $values;
  }

}
