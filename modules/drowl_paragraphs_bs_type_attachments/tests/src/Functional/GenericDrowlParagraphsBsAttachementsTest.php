<?php

namespace Drupal\Tests\drowl_paragraphs_bs_type_attachments\Functional;

use Drupal\Tests\drowl_paragraphs_bs\Functional\GenericDrowlParagraphsBsTestBase;

/**
 * Generic module tests for drowl_paragraphs_bs_type_attachments.
 *
 * @group drowl_paragraphs_bs_type_attachments
 */
class GenericDrowlParagraphsBsAttachementsTest extends GenericDrowlParagraphsBsTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'media',
    'drowl_paragraphs_bs',
    'fences',
    'entity_reference_display',
    'media_library',
    'media_library_edit',
  ];

}
