<?php

namespace Drupal\Tests\drowl_paragraphs_bs_type_simple_map\Functional;

use Drupal\Tests\drowl_paragraphs_bs\Functional\GenericDrowlParagraphsBsTestBase;

/**
 * Generic module tests for drowl_paragraphs_bs_type_simple_map.
 *
 * @group drowl_paragraphs_bs_type_simple_map
 */
class GenericDrowlParagraphsBsSimpleMapTest extends GenericDrowlParagraphsBsTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'drowl_paragraphs_bs',
    'link_attributes',
    'media',
    'media_library',
    'media_library_edit',
    'micon_link',
    'field_formatter',
  ];

}
