<?php

/**
 * @file
 * Drowl_paragraphs_bs_type_layout module.
 */

use Drupal\Core\Template\Attribute;

/**
 * Prepared variables for paragraph output.
 */

/**
 * Pass ui_styles modifier classes to the various wrappers.
 */
function drowl_paragraphs_bs_type_layout_preprocess_paragraph(&$variables) {
  if ($variables['paragraph']->bundle() == 'layout') {
    $variables['background_media_attributes'] = new Attribute();
    $variables['background_media_overlay_attributes'] = new Attribute();
    $bg_media_target_id = $variables['elements']['#paragraph']->get('field_background_media')[0] ? $variables['elements']['#paragraph']->get('field_background_media')[0]->target_id : NULL;
    $variables['has_background_media'] = !empty($bg_media_target_id);
    if (isset($variables['attributes']['class'])) {
      foreach ($variables['attributes']['class'] as $class) {
        if (str_starts_with($class, 'bg-media-')) {
          $variables['attributes']->removeClass($class);
          $variables['background_media_attributes']->addClass($class);
        }
        elseif ($variables['has_background_media'] && (str_starts_with($class, 'bg-') || str_starts_with($class, 'backdrop-filter-'))) {
          $variables['attributes']->removeClass($class);
          // Move bg-x classes to the overlay wrapper, if a bg media is present.
          $variables['background_media_overlay_attributes']->addClass($class);
        }
        elseif ($variables['has_background_media'] && (str_starts_with($class, 'text-bg-'))) {
          // Copy text-bg-x classes to the overlay wrapper, if a bg media is present.
          $variables['background_media_overlay_attributes']->addClass($class);
        }
      }
    }
  }
}

/**
 * Implements hook_theme().
 */
function drowl_paragraphs_bs_type_layout_theme($existing, $type, $theme, $path) {
  $templates = $path . '/templates';
  return [
    'paragraph__drowl_paragraphs_bs__layout' => [
      'base hook' => 'paragraph',
      'path' => $templates,
    ],
  ];
}

