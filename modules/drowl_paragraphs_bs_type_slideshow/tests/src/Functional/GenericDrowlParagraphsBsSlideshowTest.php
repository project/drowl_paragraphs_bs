<?php

namespace Drupal\Tests\drowl_paragraphs_bs_type_slideshow\Functional;

use Drupal\Tests\drowl_paragraphs_bs\Functional\GenericDrowlParagraphsBsTestBase;

/**
 * Generic module tests for drowl_paragraphs_bs_type_slideshow.
 *
 * @group drowl_paragraphs_bs_type_slideshow
 */
class GenericDrowlParagraphsBsSlideshowTest extends GenericDrowlParagraphsBsTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'drowl_paragraphs_bs',
    'media_library',
    'media',
    'drowl_media_types',
  ];

}
